<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="40"/>
        <source>Pomodoro client for Sailfish OS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="48"/>
        <source>&lt;a href=&quot;http://en.wikipedia.org/wiki/Pomodoro_Technique&quot;&gt;Wiki article&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="54"/>
        <source>Bojan &lt;gabriel9&gt; Kostic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewTask</name>
    <message>
        <location filename="../qml/pages/NewTask.qml" line="24"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/NewTask.qml" line="30"/>
        <source>Task title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/NewTask.qml" line="38"/>
        <source>Task description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="22"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="28"/>
        <source>Task time </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="40"/>
        <source>Short break </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="52"/>
        <source>Long break </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tasks</name>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="28"/>
        <source>Long pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="33"/>
        <source>Short pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="62"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="68"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="80"/>
        <source>New Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="92"/>
        <source>Tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="134"/>
        <source>Mark as finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Tasks.qml" line="134"/>
        <source>Mark as not finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
