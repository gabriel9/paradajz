<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>About</name>
    <message>
        <source>Pomodoro client for Sailfish OS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;a href=&quot;http://en.wikipedia.org/wiki/Pomodoro_Technique&quot;&gt;Wiki article&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bojan &lt;gabriel9&gt; Kostic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Show Page 2</source>
        <translation type="obsolete">Zur Seite 2</translation>
    </message>
    <message>
        <source>UI Template</source>
        <translation type="obsolete">UI-Vorlage</translation>
    </message>
    <message>
        <source>Hello Sailors</source>
        <translation type="obsolete">Hallo Matrosen</translation>
    </message>
</context>
<context>
    <name>NewTask</name>
    <message>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Task title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Task description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SecondPage</name>
    <message>
        <source>Nested Page</source>
        <translation type="obsolete">Unterseite</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Task time </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Short break </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Long break </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tasks</name>
    <message>
        <source>Long pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Short pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tasks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mark as finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mark as not finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
