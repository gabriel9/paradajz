# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-paradajz

CONFIG += sailfishapp

SOURCES += src/Paradajz.cpp

OTHER_FILES += \
    qml/cover/CoverPage.qml \
    rpm/Paradajz.changes.in \
    rpm/Paradajz.spec \
    rpm/Paradajz.yaml \
    translations/*.ts \
    qml/pages/Settings.qml \
    qml/pages/NewTask.qml \
    qml/pages/Tasks.qml \
    qml/pages/About.qml \
    harbour-paradajz.desktop \
    qml/harbour-paradajz.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/Paradajz-de.ts

RESOURCES += \
    resources.qrc

