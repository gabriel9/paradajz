import QtQuick 2.0
import Sailfish.Silica 1.0


Dialog {
    id: settings

    allowedOrientations: Orientation.Portrait | Orientation.Landscape

    property int taskTime
    property int shortBreak
    property int longBreak
    Flickable {
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id: column

            width: settings.width
            spacing: Theme.paddingSmall
            DialogHeader {
                title: qsTr("Accept")
            }
            ComboBox {
                id: settingsTaskTime
                anchors.left: parent.left
                anchors.right: parent.right
                label: qsTr("Task time ")
                currentIndex: (taskTime === 20) ? 0 : (taskTime === 25) ? 1 : (taskTime === 30) ? 2 : 1
                menu: ContextMenu {
                    MenuItem { text: "20" }
                    MenuItem { text: "25" }
                    MenuItem { text: "30" }
                }
            }
            ComboBox {
                id: settingsShortBreak
                anchors.left: parent.left
                anchors.right: parent.right
                label: qsTr("Short break ")
                currentIndex: (shortBreak === 3) ? 0 : (shortBreak === 4) ? 1 : (shortBreak === 5) ? 2 : 1
                menu: ContextMenu {
                    MenuItem { text: "3" }
                    MenuItem { text: "4" }
                    MenuItem { text: "5" }
                }
            }
            ComboBox {
                id: settingsLongBreak
                anchors.left: parent.left
                anchors.right: parent.right
                label: qsTr("Long break ")
                currentIndex: (longBreak === 10) ? 0 : (longBreak === 15) ? 1 : (longBreak === 20) ? 2 : (longBreak === 25) ? 3 : (longBreak === 30) ? 4 : 3
                menu: ContextMenu {
                    MenuItem { text: "10" }
                    MenuItem { text: "15" }
                    MenuItem { text: "20" }
                    MenuItem { text: "25" }
                    MenuItem { text: "30" }
                }
            }
        }
    }
    onDone: {
        if (result === DialogResult.Accepted) {
            taskTime = settingsTaskTime.value
            shortBreak = settingsShortBreak.value
            longBreak = settingsLongBreak.value
        }
    }
}
