import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0

Page {
    id: page
    property int gTaskTimeMinute: 25
    property int shortBreak: 5
    property int longBreak: 2
    property int jobCounter: 0
    property int maxValue: 0
    property int value: 0

    allowedOrientations: Orientation.Portrait | Orientation.Landscape

    Timer {
        id: timer
        interval: 1000
        repeat: true
        running: appWindow.running
        onTriggered: {
            if(page.value < page.maxValue)
                page.value++

            if(jobCounter !== 0 && jobCounter % 7 === 0) {
                if(page.maxValue !== longBreak*60) {
                    page.maxValue = longBreak*60
                    appWindow.titleTask = qsTr('Long pause')
                }
            } else if (jobCounter % 2 === 1) {
                if(page.maxValue !== shortBreak*60) {
                    page.maxValue = shortBreak*60
                    appWindow.titleTask = qsTr('Short pause')
                }
            } else {
                if(page.maxValue !== gTaskTimeMinute*60) {
                    findFirstActiveTask();
                    page.maxValue = gTaskTimeMinute*60
                }
            }
            if(page.maxValue - page.value === 0) {
                jobCounter++
                page.value = 0
                page.maxValue = 0
                playMusic.play()
            }
        }
    }

    SilicaListView {
        id: taskList
        anchors.fill: parent
        anchors.bottomMargin: page.isPortrait ? progressPanel.visibleSize : 0
        anchors.rightMargin: page.isPortrait ? 0 : progressPanel.visibleSize

        clip: true

        model: taskModel

        PullDownMenu {
            MenuItem {
                text: qsTr('About')
                onClicked: {
                    onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
                }
            }
            MenuItem {
                text: qsTr("Settings")
                onClicked: {
                    // TODO: when openning settings again pass values from here to it
                    var dialog = pageStack.push(Qt.resolvedUrl("Settings.qml"), {taskTime: page.gTaskTimeMinute, shortBreak: page.shortBreak, longBreak: page.longBreak})
                    dialog.accepted.connect(function() {
                        page.gTaskTimeMinute = dialog.taskTime
                        page.shortBreak = dialog.shortBreak
                        page.longBreak = dialog.longBreak
                    });
                }
            }
            MenuItem {
                text: qsTr("New Task")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("NewTask.qml"))
                    dialog.accepted.connect(function() {
                        taskModel.append({taskTitle: dialog.taskTitle, taskDescription: dialog.taskDescription, status: true});
                        findFirstActiveTask();
                    });
                }
            }
        }

        header: PageHeader {
            title: qsTr("Tasks")
        }

        ViewPlaceholder {
            enabled: taskModel.count === 0
            text: "No tasks."
        }

        delegate: ListItem {
            id: taskItemDelegate
            menu: contextMenuComponent
            width: parent.width - 2*Theme.paddingLarge
            onClicked: {
                var dialog = pageStack.push(Qt.resolvedUrl("NewTask.qml"), {taskTitle: taskTitle, taskDescription: taskDescription})
                dialog.accepted.connect(function() {
                    taskModel.set(index, {taskTitle: dialog.taskTitle, taskDescription: dialog.taskDescription, status: true});
                    findFirstActiveTask();
                });
            }

            function remove() {
                remorseAction("Deleting", function() { taskModel.remove(index) })
            }

            ListView.onRemove: animateRemoval()

            Label {
                id: taskItemTitle
                text: taskTitle
                x: Theme.paddingLarge
                width: parent.width - 2*Theme.paddingSmall
                anchors.rightMargin: Theme.paddingLarge
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: Theme.fontSizeLarge
                font.strikeout: !status
                truncationMode: TruncationMode.Fade
                color: taskItemDelegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            Component {
                id: contextMenuComponent
                ContextMenu {
                    MenuItem {
                        text: (status) ? qsTr('Mark as finished') : qsTr('Mark as not finished');
                        onClicked: {
                            taskModel.setProperty(index, 'status', !status);
                            findFirstActiveTask();
                        }
                    }
                    MenuItem {
                        text: "Remove"
                        onClicked: remove()
                    }

                }
            }
        }

        VerticalScrollDecorator {}

        ListModel {
            id: taskModel
        }
    }


    Audio {
        id: playMusic
        source: "/usr/share/sounds/jolla-ambient/stereo/battery_empty.wav"
    }

    function findFirstActiveTask() {
        for(var i = 0; i < taskModel.count; i++) {
            if(taskModel.get(i).status) {
                appWindow.titleTask = taskModel.get(i).taskTitle;
                break;
            }
        }
    }
    DockedPanel {
        id: progressPanel

        open: true

        width: page.isPortrait ? parent.width : Theme.itemSizeExtraLarge + Theme.paddingLarge
        height: page.isPortrait ? Theme.itemSizeExtraLarge + Theme.paddingLarge : parent.height

        dock: page.isPortrait ? Dock.Bottom : Dock.Right

        Column {
            //y: 20
            anchors.verticalCenter: parent.verticalCenter
            id: controllsColumn
            spacing: Theme.paddingSmall
            width: parent.width
            ProgressCircle {
                id: progressCircle
                anchors.horizontalCenter: parent.horizontalCenter
                value: page.value / page.maxValue
                IconButton {
                    id: playButton
                    icon.source: (appWindow.running) ? "image://theme/icon-m-pause" : "image://theme/icon-m-play"
                    onClicked: (appWindow.running) ? appWindow.running = false : appWindow.running = true
                    anchors.centerIn: parent
                }
            }
            Label {
                id: progressText
                anchors.horizontalCenter: parent.horizontalCenter
                text: (((page.maxValue - page.value) < 540) ? '0' : '' ) + Math.floor((page.maxValue - page.value) / 60) + ':' + ( ( ((page.maxValue - page.value) % 60).toString().length   < 2) ? '0' : '' ) + ((page.maxValue - page.value) % 60)
                onTextChanged: appWindow.timer = progressText.text
            }
        }
    }
}



