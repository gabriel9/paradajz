import QtQuick 2.0
import Sailfish.Silica 1.0


Dialog {
    id: createTask

    allowedOrientations: Orientation.Portrait | Orientation.Landscape

    property string taskTitle
    property string taskDescription

    canAccept: createTaskTitle.text

    Flickable {
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id: column

            width: createTask.width
            spacing: Theme.paddingSmall
            DialogHeader {
                title: qsTr("Accept")
            }
            TextField {
                id: createTaskTitle
                anchors.left: parent.left
                anchors.right: parent.right
                placeholderText: qsTr("Task title")
                text: taskTitle
                focus: true
            }
            TextArea {
                id: createTaskDescription
                anchors.left: parent.left
                anchors.right: parent.right
                placeholderText: qsTr("Task description")
                text: taskDescription
            }
        }
    }
    onDone: {
        if (result === DialogResult.Accepted) {
            taskTitle = createTaskTitle.text
            taskDescription = createTaskDescription.text
        }
    }
}





